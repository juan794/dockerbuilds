FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y gcc
RUN apt-get clean

WORKDIR /root/
CMD ["gcc", "-o", "app", "app.c"]