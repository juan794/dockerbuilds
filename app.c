#include <stdio.h>

void greet(char *name) {
    printf("Hello %s!\n", name);
}

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Please, indicate a name to greet\n");
        return 0;
    }

    greet(argv[1]);
    return 0;
}